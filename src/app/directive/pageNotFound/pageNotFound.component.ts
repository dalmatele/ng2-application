import { Component, OnInit} from '@angular/core';
import { ApiService } from '../../service/ApiService.service';

@Component({
  selector: 'app-notfound-component',
  templateUrl: './pageNotFound.component.html',
  styleUrls: ['./pageNotFound.component.scss'],
  providers: [ApiService]
})
export class PageNotFoundComponent implements OnInit {
  constructor(private apiService: ApiService) { }
  ngOnInit() {
  }
}
