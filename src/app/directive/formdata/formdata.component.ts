import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {ApiService} from '../../service/ApiService.service';

@Component({
  selector: 'form-data',
  templateUrl: './formdata.component.html',
  styleUrls: ['./formdata.component.scss'],
  providers: [ApiService]
})
export class FormdataComponent implements OnInit {
  @Input() formOptions: any = {
    id: 'formOptionsId',
    className: '',
    title: 'Form Options Title',
    submit: () => {
      console.log('FormdataComponent; submit form');
      console.log('this.model', this.model);
    },
    buttons: [
      {
        title: 'button 1',
        type: 'button',
        action: () => {
          console.log('button 1 click');
        }
      },
      {
        title: 'button 2', type: 'link',
        action: () => {
          console.log('link 2 click');
        },
        className: 'btn-default'
      },
    ],
    data: [
      {
        title: 'Form 1',
        className: 'col-md-6',
        form: [
          {title: 'Name', type: 'text', key: 'name', description: '', placehoder: ''},
          {title: 'Email', type: 'text', key: 'email', description: '', placehoder: ''},
          {title: 'Address', type: 'text', key: 'address', description: '', placehoder: ''},
        ]
      },
      {
        title: 'Form 2',
        className: 'col-md-6',
        form: [
          {title: 'Age', type: 'number', key: 'age', required: true, disabled: true},
          {title: 'Created At', type: 'date', key: 'createdAt', readonly: true},
        ]
      },
    ]
  };

  @Input() model: any = {
    name: 'my name nguyentvk',
    email: 'nguyentvk@uiza.io',
    address: '466/45 le van sy, p14, quan 3, hcm',
    age: 20,
    createdAt: '2018-04-04 20:20:20',
  };

  constructor(private apiService: ApiService) {
  }

  ngOnInit() {
  }
}
