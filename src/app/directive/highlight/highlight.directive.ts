import {Directive, ElementRef, HostListener, Injectable, Input} from '@angular/core';

@Injectable()
@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  @Input() highlightColor: string;
  constructor(private el: ElementRef = null) { }

  @HostListener('mouseenter') onMouseEnter() {
    this.highlight('yellow');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlight(null);
  }

  private highlight(color: string) {
    if(this.el){
      this.el.nativeElement.style.backgroundColor = color;
      this.el.nativeElement.style.cursor = 'pointer';
    }
  }
}
