import {AfterViewInit, Component, OnInit, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import { trigger, state, style, animate, transition, keyframes } from '@angular/animations';

import { fromPromise } from 'rxjs/observable/fromPromise';
import { interval } from 'rxjs/observable/interval';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { ajax } from 'rxjs/observable/dom/ajax';
import { Subscription } from 'rxjs/Subscription';
import { map, filter, switchMap } from 'rxjs/operators';
import { pipe } from 'rxjs/util/pipe';
import * as _ from 'lodash';

import { ApiService } from '../../service/ApiService.service';
import { UtilService } from '../../service/UtilService.service';

@Component({
  selector: 'heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss'],
  animations: [
    trigger('triggerNameState', [
      state('true', style({
        backgroundColor: '#43acee',
        cursor : 'pointer'
      })),
      state('false',   style({
        backgroundColor: 'transparent',
        cursor : 'pointer'
      })),
      transition('true => false', animate('.6s 100ms ease-in')),
      transition('false => true', animate('.6s 100ms ease-out'))
    ]),
    trigger('flyInOut', [
      state('in', style({transform: 'translateX(0)'})),
      transition('void => *', [
        style({transform: 'translateX(-100%)'}),
        animate('.6s 100ms')
      ]),
      transition('* => void', [
        animate('.6s 100ms', style({transform: 'translateX(100%)'}))
      ])
    ])
  ],
  providers: [ApiService]
})
export class HeroesComponent implements OnInit, AfterViewInit, OnDestroy {
  subscription : Subscription;
  public selectedHero = null;
  public heros: any[];
  constructor(private apiService: ApiService,
              private _router: Router,
              private _utilService:UtilService
  )
  {
  }

  toggleState(item:any){
    item.status = !item.status;
    console.log(item);
  }
  ngOnInit() {
    let param = {};
    this.subscription = this.apiService.getListPost(param).subscribe((response:any)=>{
      console.log('this.apiService.getListPost().subscribe',response);
      this.heros = response;
    },error=>{
      console.error(error);
    });
  }
  ngAfterViewInit(){}

  onSelect(hero: any): void {
    this.selectedHero = hero;
    this._router.navigate(['/hero', this.selectedHero.id ]);
  }

  deleteHero(id:any, index:any):void{
    if(!confirm('Are you sure?')) return;
    console.log(id);
    console.log(index);
    let param = {id :id};
    this.subscription = this.apiService.deleteHero(param).subscribe((response:any)=>{
      console.log('this.apiService.deleteHero',response);
      this.heros.splice(index,1);
    },error=>{
      console.error('deleteHero=',error);
      this._utilService.notifyError(error,'');
    });
  }
  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
}
