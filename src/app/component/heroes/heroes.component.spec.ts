import { HeroesComponent } from './heroes.component';

describe('HeroesComponent', () => {
  let component = new HeroesComponent(null, null, null);
  beforeEach(function () {
  });
  it('should create an instance', () => {
    expect(component).toBeTruthy();
  });
  it('HeroesComponent should has function delete Hero', () => {
    expect(typeof(component.deleteHero)).toEqual('function');
  });
});
