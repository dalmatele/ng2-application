import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../service/ApiService.service';
import { UtilService } from '../../service/UtilService.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.scss']
})
export class HeroDetailComponent implements OnInit, OnChanges, OnDestroy {
  private subscription: Subscription;
  @Input() heroId: any;
  public id :any;
  public  hero :any;
  public formOptions:any = {
    id : 'formheroid',
    submit : ()=>{
      console.log('submit form; this.hero');
      console.log('this.hero', this.hero);
      this.subscription = this._apiService.updateHero(this.hero).subscribe((res)=>{
        console.log('updateHero',res);
        this.hero = res;
        this._utilService.notifySuccess('Success');
      },error=>{
        console.error(error);
        this._utilService.notifyError(JSON.stringify(error));
      });
    },
    data : [
      {
        title:'Hero Detail',
        className : 'col-md-12',
        form : [
          {title: 'Name', type: 'text', key : 'name', description:'description name', placehoder : ''},
          {title: 'Email', type: 'text', key : 'email', description:'description email', placehoder : ''},
          {title: 'Description', type: 'textarea', key : 'description', rows: 10},
          {title: 'Address', type: 'text', key : 'address', description:'', placehoder : ''},
          {title: 'Created At', type: 'date', key : 'createdAt', readonly : true},
        ]
      }
    ]
  };
  constructor(private _apiService : ApiService, private _activedRoute: ActivatedRoute, private  _utilService: UtilService) {}

  getPost(id:any){
    this.subscription = this._apiService.getPostDetail({id: id}).subscribe((res)=>{
      console.log('getPostDetail',res);
      this.hero = res;
    },error=>{
      console.error(error);
    });
  }

  ngOnInit(): void {
    this.id = this._activedRoute.snapshot.paramMap.get('id');
    this.getPost(this.id);
  }

  ngOnChanges(changes: any) {
  }
  ngOnDestroy() {
    // prevent memory leak when component destroyed
    this.subscription.unsubscribe();
    console.log('hero detail ngOnDestroy');
  }
}
