import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../service/ApiService.service';

@Component({
  selector: 'hero-detail-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class HeroDetailOverviewComponent implements OnInit, OnChanges {
  constructor(private apiService : ApiService, private _activedRoute: ActivatedRoute) {
  }
  ngOnInit(): void {
  }
  ngOnChanges(changes: any) {
  }
}
