import {Routes, RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {HomeComponent} from './component/home/home.component';
import {HeroesComponent} from './component/heroes/heroes.component';
import {HeroDetailComponent} from './component/hero-detail/hero-detail.component';
import {HeroDetailOverviewComponent} from './component/hero-detail/overview/overview.component';
import {HeroDetailSkillComponent} from './component/hero-detail/skill/skill.component';
import {PageNotFoundComponent} from './directive/pageNotFound/pageNotFound.component';
import {LoginComponent} from './component/login/login.component';

const routing: Routes = [
  {path: '', component: HomeComponent},
  {path: 'heros', component: HeroesComponent},
  {path: 'login', component: LoginComponent},
  {
    path: 'hero/:id', component: HeroDetailComponent,
    children: [
      {path: '', redirectTo: 'overview', pathMatch: 'full'},
      {path: 'overview', component: HeroDetailOverviewComponent},
      {path: 'skill', component: HeroDetailSkillComponent}
    ]
  },
  {path: '**', component: PageNotFoundComponent},

];
export const appRoutes = RouterModule.forRoot(
  routing,
  {enableTracing: false} // <-- debugging purposes only
);
