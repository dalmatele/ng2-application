import {Component} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToasterModule, ToasterService, ToasterConfig,ToasterContainerComponent} from 'angular2-toaster';
import {UtilService} from './service/UtilService.service';
import {Routes, RouterModule} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent
{
  //https://www.npmjs.com/package/angular2-toaster
  public configToaster: any = new ToasterConfig({
    showCloseButton: true,
    tapToDismiss: true,
    timeout: 60*1000,
    animation: 'flyRight', //'fade', 'flyLeft', 'flyRight', 'slideDown', and 'slideUp'.
    limit: 10,
    // positionClass: 'my-toast-class'
  });
  constructor() {}
}
