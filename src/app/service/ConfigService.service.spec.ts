import { ConfigService } from './ConfigService.service';

describe('ConfigService', () => {
  let service;
  beforeEach(function () {
    service = new ConfigService();
  });

  it('ConfigService should has apiUrl link', () => {
    expect(ConfigService.apiUrl).toEqual('http://5ac632b8a79a110014ce6831.mockapi.io/api');
  });

  it('ConfigService should has function getConfig ', () => {
    expect(typeof(service.getConfig)).toEqual('function');
  });

});
